#!/usr/bin/env python

import sys
from Bio import SeqIO
from optparse import OptionParser
from numpy import  mean

def main():
    usage = "usage: %prog [options] input_fasta_file"
    parser = OptionParser(usage=usage)
    parser.add_option("-f", "--quality-file", dest="qualityFile", help="Quality file name", type="string", default="")
    parser.add_option("-q", "--qual", dest="quality", help="Mean quality for filtering", type="int", default=30)
    parser.add_option("-l", "--length", dest="length", help="Minimum length for filtering", type="int", default=100)
    parser.add_option("-c", "--count", dest="count", action="store_true", help="Return the number of filtered sequences")
    parser.add_option("-o", "--output", dest="output", help="Output file name", type="string", default="out.fasta")

    (options, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("Invalid number of arguments")

    if not options.qualityFile:
        qualityFile = args[0] + ".qual"

    try:
        seqsHandler = open(args[0])
    except:
        sys.exit("Could not open file: %s\n" % args[0])

    try:
        qualityHandler = open(qualityFile)
    except:
        sys.exit("Could not open file: %s\n" % args[0])

    seqsDict = SeqIO.to_dict(SeqIO.parse(seqsHandler, "fasta"))
    qualsDict = SeqIO.to_dict(SeqIO.parse(qualityHandler, "qual"))

    seqKeys =  set(seqsDict.keys())
    qualKeys = set(qualsDict.keys())
    if not seqKeys == qualKeys:
        sys.exit("The identifiers of the fasta and quality files does not match\n")


    filteredSeqs = 0
    if not options.count:
        outFile = open(options.output, "w")
        outQual = open(options.output + ".qual", "w")
    for name in seqsDict.keys():
        seq = seqsDict.get(name)
        length = len(seq)
        qual = qualsDict.get(name)
        qualities = qual.letter_annotations["phred_quality"]
        meanQuality = mean(qualities)
        if length >= options.length and meanQuality >= options.quality:
            if not options.count:
                #print name, length, meanQuality
                outFile.write(seq.format("fasta"))
                outQual.write(qual.format("qual"))
            filteredSeqs += 1

    if not options.count:
        outFile.close()
        outQual.close()


    if options.count:
        print filteredSeqs



if __name__ == "__main__":
    main()

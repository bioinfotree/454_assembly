#!/usr/bin/env python

import sys
from Bio import SeqIO
from django.template import Template, Context
from django.template.loader import get_template
from django.conf import settings
import os
from optparse import OptionParser
from numpy import median, mean
import rpy2.robjects as robjects

settings.configure(TEMPLATE_DIRS=(os.path.join(os.environ["FOURFIVEFOURPATH"], "scons"),), DEBUG=True, TEMPLATE_DEBUG=True)

def lenStats(fastaSeqs):
    lens = [len(seq.seq) for seq in fastaSeqs]
    med = median(lens)
    average = mean(lens)
    return average,med


def plotContigsQualVsLen(qualities, lens, filename):
    qualitiesVector = robjects.FloatVector(qualities)
    lensVector = robjects.IntVector(lens)
    r_setwd = robjects.r("setwd")
    r_setwd(os.getcwd())
    r_source = robjects.r("source")
    r_source(os.path.join(os.environ["FOURFIVEFOURPATH"], "tools", "reads_contigs_stats.R"))

    r_contig_quals_vs_len = robjects.r("contig_quals_vs_len")
    r_contig_quals_vs_len(qualitiesVector, lensVector, filename)



def plotReadsMeanQualityHistogram(readMeanQuals, pdfName):
    readMeanQualsVector = robjects.IntVector(readMeanQuals)
    r_setwd = robjects.r("setwd")
    r_setwd(os.getcwd())
    r_source = robjects.r("source")
    r_source(os.path.join(os.environ["FOURFIVEFOURPATH"], "tools", "reads_contigs_stats.R"))

    r_mean_qual_histogram = robjects.r("mean_qual_histogram")
    r_mean_qual_histogram(readMeanQualsVector, "mira_first_mean_quality_hist.pdf")


def readStatistics(inputFile):
    inputReadQuals = list(SeqIO.parse(open(inputFile + ".qual"), "qual"))
    reads = len(inputReadQuals)
    readMeanQuals = []
    lens = []
    for seq in inputReadQuals:
        quals = seq.letter_annotations["phred_quality"]
        lens.append(len(seq))
        readMeanQuals.append(mean(quals))
    averageQuality = mean(readMeanQuals)
    averageLength = mean(lens)
    medianQuality = median(readMeanQuals)
    medianLength = median(lens)
    d = {"qualities":readMeanQuals, "lengths":lens, "averageQuality":averageQuality, "medianQuality":medianQuality, "averageLength":averageLength, "medianLength":medianLength, "numberOfReads":reads}
    return d

def plotRawVsTrimmedBoxplot(rawData,trimmedData, outputName ):
    rawVector = robjects.IntVector(rawData)
    trimmedVector = robjects.IntVector(trimmedData)

    r_setwd = robjects.r("setwd")
    r_setwd(os.getcwd())
    r_source = robjects.r("source")
    r_source(os.path.join(os.environ["FOURFIVEFOURPATH"], "tools", "reads_contigs_stats.R"))
    r_rawVsTrimmedBoxplot = robjects.r("raw_vs_trimmed_boxplot")
    r_rawVsTrimmedBoxplot(rawVector, trimmedVector, outputName)



def main():
    usage ="usage: %prog [options] input_fasta_file mira_results_directory"
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--prefix", dest="prefix", help="Prefix used by mira project", type="string", default="transcriptome")
    parser.add_option("-s", "--section-only", dest="section", help="Create only section not entire document", action="store_true")

    (options, args) = parser.parse_args()
    if len(args) < 2:
        parser.error("Tow few arguments")
    prefix = options.prefix


    inputFile = args[0]
    miraResultsDirectory = args[1]
    dictionaryForContext = {}

    if options.section:
        alldocument = False
    else:
        alldocument = True

    #List containing mean qualities for reads passed to mira
    readStats = readStatistics(inputFile)
    readMeanQuals = readStats["qualities"]
    readLengths = readStats["lengths"]
    readsLenAverage = readStats["averageLength"]
    readsLenMedian = readStats["medianLength"]
    readsQualAverage = readStats["averageQuality"]
    readsQualMedian = readStats["medianQuality"]
    #Plots the histogram of mean quality for reads passed to mira
    plotReadsMeanQualityHistogram(readMeanQuals, "mira_first_mean_quality_hist.pdf")


    r_average_qual_dist = robjects.r("average_qual_dist")
    readMeanQualsVector = robjects.IntVector(readMeanQuals)
    r_average_qual_dist(readMeanQualsVector)

    numberOfInputReads = readStats["numberOfReads"]
    #dictionaryForContext["numberOfInputReads"] = numberOfInputReads
    #dictionaryForContext["readsLenAverage"] = readsLenAverage
    #dictionaryForContext["readsLenMedian"] = readsLenMedian

    miraResultsDirectory = os.path.join(miraResultsDirectory, prefix + "_d_" + "results" )
    
    assemblyFile = os.path.join(miraResultsDirectory, prefix + "_out.padded.fasta")
    assemblyHandler = open(assemblyFile)
    assemblyQualityHandler = open(assemblyFile + ".qual")
    contigsQuals = list(SeqIO.parse(assemblyQualityHandler, "qual"))
    contigs = list(SeqIO.parse(assemblyHandler, "fasta"))
    contigLengths = [len(contig) for contig in contigs]
    contigMeanQuals = [mean(contig.letter_annotations["phred_quality"]) for contig in contigsQuals]
    contigMeanQual = mean(contigMeanQuals)
    contigMedianQual = median(contigMeanQuals)
    plotContigsQualVsLen(contigMeanQuals, contigLengths, "mira_first_contigs_quality_vs_len.pdf")
    numberOfContigs = len(contigs)
    contigsLenAverage,contigsLenMedian = lenStats(contigs)
    #dictionaryForContext["contigsLenAverage"] = contigsLenAverage
    #dictionaryForContext["contigsLenMedian"] = contigsLenMedian
    #dictionaryForContext["numberOfContigs"] = len(contigs)

    #metaAssemblyQualityHandler = open("final_assembly.fasta" + ".qual")
    #metaContigQuals = list(SeqIO.parse(open("final_assembly.fasta.qual"), "qual"))
    #metaContigs = list(SeqIO.parse(open("final_assembly.fasta"), "fasta"))
    #metaContigLengths = [len(contig) for contig in metaContigs]
    ##sys.stderr.write("\n")
    #metaContigMeanQuals = [mean(contig.letter_annotations["phred_quality"]) for contig in metaContigQuals]
    #metaContigMeanQual = mean(metaContigMeanQuals)
    #metaContigMedianQual = median(metaContigMeanQuals)
    ##sys.stderr.write(str(len(metaContigQuals)))
    ##sys.stderr.write("\n")
    ##sys.stderr.write(str(len(metaContigLengths)))
    ##sys.stderr.write("\n")
    #plotContigsQualVsLen(metaContigMeanQuals, metaContigLengths, "mira_second_contigs_quality_vs_len.pdf")
    #numberOfMetaContigs = len(metaContigs)
    #metaContigsLenAverage,metaContigsLenMedian = lenStats(metaContigs)
 


    t = get_template("report")
    context = Context(locals())
    print t.render(context)

    
    

if __name__ == "__main__":
    main()

#!/usr/bin/env python

from Bio import SeqIO
import numpy as np
import matplotlib.pyplot as pp
from optparse import OptionParser


def meanQualityValues(handler):
    records = list(SeqIO.parse(handler, "qual"))
    means = []
    for record in records:
        quals = record.letter_annotations["phred_quality"]
        mean = np.mean(quals)
        means.append(mean)
    return means

def main():
    usage = "usage: %prog [options] fastaFiles..."
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--plot", dest="plot", action="store_true", help="Draws a boxplot")
    (options, args) = parser.parse_args()
    if len(args) < 1:
        parser.error("Incorrect number of arguments")
 
    data = []
    for file in args:
        print file + ":"
        f = open(file)
        means = meanQualityValues(f)
        mean = np.mean(means)
        median = np.median(means)
        std = np.std(means)
        print "Mean:    %20f" % mean
        print "Median:  %20f" % median
        print "Std dev: %20f" % std
        data.append(means)
        print
    if options.plot == True:
        pp.boxplot(data)
        pp.show()

if __name__ == "__main__":
    main()

#!/usr/bin/env Rscript
# to execute: Rscript /home/michele/Research/Projects/Transcriptomic_analysis/Src/boxplot.R
# or R < source("/home/michele/Research/Projects/Transcriptomic_analysis/Src/boxplot.R")


#Start PNG device driver to save output to figure.png
#png(filename="/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA1_11_2010/raw_seq_analysis/box_plot.png", height=295, width=300, bg="white")

png("box_plot.png", height=480, width=480)

# Start PDF device driver to save output to figure.pdf
#pdf(file="C:/R/figure.pdf", height=3.5, width=5)


# Read values from tab-delimited files
raw_reads_1 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA1_11_2010/raw_reads_analysis/raw_reads_length.txt", header=FALSE, sep="\t")

trimmed_reads_1 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA1_11_2010/raw_reads_analysis/trimmed_reads_length.txt", header=FALSE, sep="\t")





raw_reads_2 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA2_11_2010/raw_reads_analysis/raw_reads_length.txt", header=FALSE, sep="\t")

trimmed_reads_2 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA2_11_2010/raw_reads_analysis/trimmed_reads_length.txt", header=FALSE, sep="\t")




raw_reads_3 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA3_11_2010/raw_reads_analysis/raw_reads_length.txt", header=FALSE, sep="\t")

trimmed_reads_3 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA3_11_2010/raw_reads_analysis/trimmed_reads_length.txt", header=FALSE, sep="\t")




raw_reads_4 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA4_11_2010/raw_reads_analysis/raw_reads_length.txt", header=FALSE, sep="\t")

trimmed_reads_4 <- read.table("/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-09-29/CDNA4_11_2010/raw_reads_analysis/trimmed_reads_length.txt", header=FALSE, sep="\t")


#dim(raw_reads)
#class(raw_reads)

raw_reads_vector_1<- c(as.numeric(as.matrix(raw_reads_1)[,2]))
trimmed_reads_vector_1<- c(as.numeric(as.matrix(trimmed_reads_1)[,2]))

raw_reads_vector_2<- c(as.numeric(as.matrix(raw_reads_2)[,2]))
trimmed_reads_vector_2<- c(as.numeric(as.matrix(trimmed_reads_2)[,2]))


raw_reads_vector_3<- c(as.numeric(as.matrix(raw_reads_3)[,2]))
trimmed_reads_vector_3<- c(as.numeric(as.matrix(trimmed_reads_3)[,2]))


raw_reads_vector_4<- c(as.numeric(as.matrix(raw_reads_4)[,2]))
trimmed_reads_vector_4<- c(as.numeric(as.matrix(trimmed_reads_4)[,2]))


#class(raw_reads_vector)
#class(trimmed_reads_vector)


total_vector <- list(raw_reads_vector_1, trimmed_reads_vector_1,
                  raw_reads_vector_2, trimmed_reads_vector_2,
                  raw_reads_vector_3, trimmed_reads_vector_3,
                  raw_reads_vector_4, trimmed_reads_vector_4)

# names: 	group labels which will be printed under each boxplot. Can be a character vector or an expression (see plotmath).
#boxplot(total_vector, names=c("raw","trimmed","raw","trimmed","raw","trimmed","raw","trimmed"))

# outline = TRUE point out of the extreme values
boxplot(total_vector, xaxt="n", outline = FALSE)

# Create a title with a red, bold/italic font
title(main="Reads length before and after pre-processing for each library", font.main=4)

# Make x axis using Mon-Fri labels
axis(1, at=c(1:8), lab=c("raw\nCDNA1", "trimmed\nCDNA1","raw\nCDNA2", "trimmed\nCDNA2","raw\nCDNA3", "trimmed\nCDNA3","raw\nCDNA4", "trimmed\nCDNA4"), las=3)


# Label the x and y axes with dark green text
#title(xlab="Days", col.lab=rgb(0,0.5,0))
title(ylab="Reads length", col.lab=rgb(0,0.5,0))


# Turn off device driver (to flush output to png)
dev.off()


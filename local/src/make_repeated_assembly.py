#!/usr/bin/python
# make_repeated_assembly.py --- 
# 
# Filename: make_repeated_assembly.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Feb  8 18:04:09 2011 (+0100)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# miscellaneous operating system interfaces
import sys,os,argparse
# class for running mira assembler
from MiraAssembler import MiraAssembler
# container class of utils
from PipeUtils import PipeUtil
# class for remapping initial reads on metacontigs from a second MIRA assembly
from OnMiraAssemblyRemapper import OnMiraAssemblyRemapper


def main(arguments):

    sys.argv = arguments
    args = arg_parse()

    # FASTA (TRACE_INFO, STRAIN) MIRA
    # name of input fasta file
    in_reads_fasta = args.fasta
    # name of input quality file
    in_reads_qual = args.qual
    # name of input xml file
    in_reads_xml = args.xml
    # strain file
    in_reads_strain = args.strain
    # numeber of assembly cicles
    cycles = args.cycles
    # numeber of cpus used by
    # all the programs
    cpus = args.cpus
    # project name prefix
    prj_name = args.prj_name

    # # TEST FILES
    # in_path = r""
    # in_reads_fasta = r"/home/michele/Research/Projects/Transcriptomic_analysis/Data/454_test/in.fasta.in_newbler"
    # in_reads_qual = r"/home/michele/Research/Projects/Transcriptomic_analysis/Data/454_test/in.fasta.in_newbler.qual"
    # in_reads_xml = r"/home/michele/Research/Projects/Transcriptomic_analysis/Data/454_test/fake_in.xml"
    # in_reads_strain = r"/home/michele/Research/Projects/Transcriptomic_analysis/Data/454_test/CDNA3-4_straindata_in.txt"
    # cycles = int(2)


    # use pass values to track the path of the initial reads.
    # Serves in the mapping stage.
    in_seq = in_reads_fasta
    in_qual = in_reads_qual
    in_xml = in_reads_xml
    in_strain = in_reads_strain

    results_dir = r""
    proj_dir = r""
    assembly_dir = r""

    fasta_lnk = r""
    qual_lnk = r""
    xml_lnk = r""
    strain_lnk = r""

    mira_settings = r""
    debris_fasta = r""
    debris_qual = r""

    mira_assembler = MiraAssembler()
    current_path = os.getcwd()

    # list containing dictionary
    # of variable = path_name
    # for the current assembly project
    log_lst = []

    for i in range(cycles):
        # dictionary of "variable = path_name"
        # for the current assembly cycle
        log_dict = {}
        ####  MIRA PARAMETERS ####
        proj_name = r"%s_%i" %(prj_name, i)
        ####  MIRA PARAMETERS ####
        log_dict["proj_name"] = proj_name
        
        # set values for all assembly
        results_dir_name = r"assembly_%i" % i
        log_dict["results_dir_name"] = results_dir_name
        mira_assembler.set_results_dir(results_dir_name)
        mira_assembler.set_proj_name(proj_name)

        ####  MIRA PARAMETERS ####
        # sets cpus for MIRA
        mira_assembler.set_cpus(cpus)
        ####  MIRA PARAMETERS ####
        
        if True :
            if i <= 1:               
                # prepare input files for 454
                fasta_lnk, qual_lnk, xml_lnk, strain_lnk = mira_assembler.set_input_fasta(fasta_file=in_seq, qual_file=in_qual, xml_file=in_xml, straindata_file=in_strain, tech=r"454")

            elif i > 1:
                # marge together precedent contigs + precedent debris
                fasta_lnk, qual_lnk, xml_lnk, strain_lnk = mira_assembler.set_input_multifasta(r"454", (in_seq, in_qual), (debris_fasta, debris_qual))


                              
            # save values
            log_dict["fasta_lnk"] =  fasta_lnk
            log_dict["qual_lnk"] =  qual_lnk
            log_dict["xml_lnk"] =  xml_lnk
            log_dict["strain_lnk"] = strain_lnk
            # I see debris fasta and qual assembled together with other sequences,
            # in the current assembly
            log_dict["debris_fasta"] = debris_fasta
            log_dict["debris_qual"] = debris_qual


            if i == 0:
                ####  MIRA PARAMETERS FIRST CYCLE ####
                # yes singlets on the first run !!!REMEMBER TO CHANGE FROM DRAFT TO ACCURATE!!!
                
                # mira commands
        
                # -sssip: Default is no. Controls whether unimportant singlets
                #         are written to the result files
                # -fo: Default is no. File check only
                # -cpat: clip polyAT
                # -qc: MIRA own quality clipping
                # -ace: automatic contig editing (strong suggested on for 454 data)
                # -fnicpst: force nonIUPACconsensus per sequence type
                # -not: cpu number
                # -mxti: use xml traceinfo file
                # -fai: path of fasta file
                # -fqui: path of fasta quality file
                # -xtii: path of xml traceinfo file
                # -AS:mrpc=1: defines the minimum number of reads a contig must have before it
                #             is built or saved by MIRA
                # -SB:lsd=yes: straindata is a key value file, one read per line. First the name
                #              of the read, then the strain name of the organism the read comes from.
                mira_settings = mira_assembler.set_mira_settings([u'-job=denovo,est,accurate,454', r'-fasta'], [], _COMMON_SETTINGS=[u'-LR:fo=no', u"-SB:lsd=no"],  _454_SETTINGS=[u"-CL:cpat=0:qc=0", "-ED:ace=1" ,u"-OUT:sssip=yes", u"-CO:fnicpst=yes", u"-LR:mxti=no", u"-AS:mrpc=1"])
                ####  MIRA PARAMETERS FIRST CYCLE ####

            elif i >= 1:
                ####  MIRA PARAMETERS OTHER CYCLES ####
                mira_settings = mira_assembler.set_mira_settings([u'-job=denovo,est,accurate,454', r'-fasta', r"--notraceinfo"], [], _COMMON_SETTINGS=[u'-LR:fo=no'],  _454_SETTINGS=[u"-CL:cpat=0:qc=0", "-ED:ace=1" ,u"-OUT:sssip=yes", u"-CO:fnicpst=yes", u"-LR:mxti=no", u"-AS:mrpc=1"])
                ####  MIRA PARAMETERS OTHER CYCLES ####
                
            # save values
            log_dict["mira_settings"] =  mira_settings

            # run mira assembler
            results_dir, proj_dir, assembly_dir = mira_assembler.assembly()
            
            # save values
            log_dict["results_dir"] =  results_dir
            log_dict["proj_dir"] =  proj_dir
            log_dict["assembly_dir"] =  assembly_dir

            # assembly sequences and quality
            new_seq = os.path.join(assembly_dir, r"%s_out.unpadded.fasta" % proj_name)
            new_qual = os.path.join(assembly_dir, r"%s_out.unpadded.fasta.qual" % proj_name)
            # save values
            log_dict["new_seq"] =  new_seq
            log_dict["new_qual"] =  new_qual


            if i >= 1:

                # change dir to project dir
                os.chdir(proj_dir)

                # get fasta and qual from debrise list.
                # As input uses link to the fasta and input files
                # for MIRA prepared by the MiraAssembler().set_input_fasta and
                # MiraAssembler().set_input_multifasta methods
                debris_fasta, debris_qual = get_mira_debris(mira_assembler, proj_dir, \
                                                            fasta_lnk, qual_lnk)
                # save values
                log_dict["debris_fasta"] =  debris_fasta
                # save values
                log_dict["debris_qual"] = debris_qual 


                # Added to the file *_info_contigreadlist.txt sequences in the file
                # *_info_debrislist.txt so that they appear as sequences composed by themselves
                debris_to_contigreadlist(mira_assembler, proj_dir)

                # change to current path
                os.chdir(current_path)
            
                # performs only from the second cycle onwards,
                # as you must have a first assembly concluded.
            
                # take values from previous assembly
                old_proj_dir = log_lst[i-1]["proj_dir"]
                old_proj_name = log_lst[i-1]["proj_name"]
                # track back reads from previous assembly on metacontigs of the current assembly
                metacontig2reads_remapping(mira_assembler, old_proj_dir, old_proj_name, proj_dir)

                
        else:
            pass
        ##     # prepare input files for sanger
        ##     fasta_lnk, qual_lnk, xml_lnk = mira_assembler.set_input_fasta(fasta_file=in_seq, qual_file=in_qual, tech=r"sanger")
        ##     log_dict["fasta_lnk"] =  fasta_lnk
        ##     log_dict["qual_lnk"] =  qual_lnk
        ##     log_dict["xml_lnk"] =  xml_lnk
        ##     # yes singlets on the other runs with sanger settings
        ##     mira_settings = mira_assembler.set_mira_settings([u'-job=denovo,est,accurate,sanger', r'-fasta', r"--notraceinfo"], [], _COMMON_SETTINGS=[u'-LR:fo=no'],  _SANGER_SETTINGS=[u"-CL:cpat=0:qc=0", "-ED:ace=1" ,u"-OUT:sssip=yes", u"-CO:fnicpst=yes", u"-LR:mxti=no"])
        ##     log_dict["mira_settings"] =  mira_settings
        ##     results_dir, proj_dir, assembly_dir = mira_assembler.assembly()
        ##     log_dict["results_dir"] =  results_dir
        ##     log_dict["proj_dir"] =  proj_dir
        ##     log_dict["assembly_dir"] =  assembly_dir
        ##     new_seq = os.path.join(assembly_dir, r"%s_out.unpadded.fasta" % proj_name)
        ##     new_qual = os.path.join(assembly_dir, r"%s_out.unpadded.fasta.qual" % proj_name)


        # current contigs in *_out.unpadded.fasta and their
        # quality in *_out.unpadded.fasta.qual become input
        # for next round of assembly
        in_seq = new_seq
        in_qual = new_qual
        # we don't need xml tracenfo in assemblies
        # after the first
        in_xml = r""
        # we don't need straindatafile in assemblies
        # after the first
        in_strain = r""
        
        # append to list dictionary of current
        # assembly path and name values
        log_lst.append(log_dict)

        print "-"*10
        print "\nDONE %i ROUND!\n" % i
        print "-"*10
        
    # remaps the initial reads on metacontig obtained at the end of each cycle of assembly
    # after the first.
    # Use SSAHA2.
    for i in range(1, len(log_lst)):
        remaps_dir, tot_fasta, tot_qual = remapping(result_dir_name=log_lst[i]["results_dir_name"], \
                                                    proj_dir=log_lst[i]["proj_dir"], \
                                                    proj_name=log_lst[i]["proj_name"], \
                                                    contigs_fasta = log_lst[i]["new_seq"], \
                                                    contigs_qual = log_lst[i]["new_qual"], \
                                                    debris_fasta = log_lst[i]["debris_fasta"], \
                                                    debris_qual = log_lst[i]["debris_qual"], \
                                                    reads_fasta = in_reads_fasta, \
                                                    reads_qual = in_reads_qual)
        
        
    # write path and name values of all cicles
    file_handle = open("statistics.txt", "w")
    file_handle.write("-"*20)
    file_handle.write("SUMMARY:")
    file_handle.write("-"*20)
    for (index, elem) in enumerate(log_lst):
        file_handle.write("\nROUND %i\n" % index)
        for key in elem.keys():
            file_handle.write("%i\t%s\t%s\n" %(index, key, elem[key]))
        file_handle.write("\n\n")
    file_handle.close()
        
    return 0





def get_mira_debris(mira_assembler, proj_dir, fasta_lnk, qual_lnk):

    print "-"*10
    print "\nMINING DEBRIS...\n"
    print "-"*10

    # get fasta and qual from debrise list. As input uses link to the fasta and input files
    # for MIRA prepared by the MiraAssembler().set_input_fasta and
    # MiraAssembler().set_input_multifasta methods
    debris_fasta, debris_qual = mira_assembler.get_mira_debris(proj_dir, fasta_lnk, qual_lnk)

    return debris_fasta, debris_qual




def metacontig2reads_remapping(mira_assembler, old_proj_dir, old_proj_name, proj_dir):

    print "-"*10
    print "\nREMAPPING READS ON METACONTIGS...\n"
    print "-"*10

    # track back reads from previous assembly on metacontigs of the current assembly
    mira_assembler.metacontig2reads_tracer(old_proj_dir=old_proj_dir, old_proj_name=old_proj_name, new_proj_dir=proj_dir)
    
    return 0
    


def debris_to_contigreadlist(mira_assembler, proj_dir):
    # Added to the file *_info_contigreadlist.txt sequences in the file *_info_debrislist.txt
    # so that they appear as sequences composed by themselves
    mira_assembler.debris_to_contigreadlist(proj_dir)
    
    return 0



def remapping(result_dir_name, proj_dir, proj_name, contigs_fasta, \
              contigs_qual, debris_fasta, debris_qual, reads_fasta, reads_qual):

    print "-"*10
    print "\nFINAL REMAPPING...\n"
    print "-"*10

    on_mira_assembly_remapper = OnMiraAssemblyRemapper()
    remaps_dir, tot_fasta, tot_qual = on_mira_assembly_remapper.remapping(result_dir_name, proj_dir, proj_name, contigs_fasta, \
                                     contigs_qual, debris_fasta, debris_qual, reads_fasta, reads_qual)
    
    return remaps_dir, tot_fasta, tot_qual





def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="This program performs iterative cycles of assembly using MIRA assembler. For all assembly cycles after the first, initial reads are remapped on metacontigs using SSAHA2. The script must be changed at code level to set custom parameters to be passed to MIRA assembler.")
    parser.add_argument("--fasta", "-f", dest="fasta", type=str, required=True, help="Full path to a FASTA format file containing the sequences of reads to be assembled")
    parser.add_argument("--qual", "-q", dest="qual", type=str, required=False, help="Full path to a FASTA QUALITY format file containing the quality of reads to be assembled")
    parser.add_argument("--xml", "-x", dest="xml", type=str, required=False, help="Full path of a XML TRACEINFO file containing the cutting information of the reads to assembled")
    parser.add_argument("--strain", "-s", dest="strain", type=str, required=False, help="Full path of a file in the STRAINDATA format for MIRA assembler. See MIRA documentation")
    parser.add_argument("--cycles", "-c", dest="cycles", default=1, required=False, type=int, help="Number of assembly cycles. Default is 1. The remap of reads on metacontig is carried out only from the second assembly cycle onwards")
    parser.add_argument("--cpus", "-u", dest="cpus", default=1, required=False, type=int, help="Number of cpus used by MIRA and other programs")
    parser.add_argument("--prj", "-p", dest="prj_name", default="prj_name", required=False, type=str, help="Prefix added to MIRA project name")
    
    args = parser.parse_args()
    return args




# execute the main function
if __name__ == "__main__":
    main(sys.argv)


# 
# make_repeated_assembly.py ends here

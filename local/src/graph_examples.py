# graph_examples.py --- 
# 
# Filename: graph_examples.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Thu Jun 10 10:54:16 2010 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

## The new ticker code was designed to explicity support user customized
## ticking.  The documentation
## http://matplotlib.sourceforge.net/matplotlib.ticker.html details this
## process.  That code defines a lot of preset tickers but was primarily
## designed to be user extensible.

## In this example a user defined function is used to format the ticks in
## millions of dollars on the y axis

# saving your plot for posterity
#import matplotlib.pyplot as plt

## Syntax:
#
# savefig(fname, dpi = None, facecolor = 'w', edgecolor = 'w', orientation = 'portrait', papertype = None, format = None, transparent = False)
# An Example:
# import matplotlib.pyplot as plt
# plot.savefig('example.png', format = 'png')



#from matplotlib.ticker import FuncFormatter
#from pylab import *

## x =  arange(4)
## money = [1.5e5, 2.5e6, 5.5e6, 2.0e7]

## def millions(x, pos):
##     'The two args are the value and tick position'
##     return '$%1.1fM' % (x*1e-6)

## formatter = FuncFormatter(millions)

## ax = subplot(111)
## ax.yaxis.set_major_formatter(formatter)
## bar(x, money)
## xticks( x + 0.5,  ('Bill', 'Fred', 'Mary', 'Sue') )
## #show()

## #import matplotlib.pyplot as plt
## plt.savefig('example.pdf', format ='pdf')



## from mpl_toolkits.mplot3d import Axes3D  
## import matplotlib.pyplot as plt  
## import numpy as np  

## fig = plt.figure()  
## ax = Axes3D(fig)  
## for c, z in zip(['r', 'g', 'b', 'y'], [30, 20, 10, 0]):
##     xs = np.arange(20)
##     ys = np.random.rand(20)
##     ax.bar(xs, ys, zs=z, zdir='y', color=c, alpha=0.8)

## ax.set_xlabel('X')
## ax.set_ylabel('Y')
## ax.set_zlabel('Z')
## plt.show()




## from matplotlib import pylab
## pylab.bar([0, 1], [1.0, 2.0])
## pylab.bar([0, 1], [2.0, 1.0])

## # loop through all patch objects and collect ones at same x
## all_patches = pylab.axes().patches
## patch_at_x = {}
## for patch in all_patches:
##     if patch.get_x() not in patch_at_x: patch_at_x[patch.get_x()] = []
##     patch_at_x[patch.get_x()].append(patch)

## # custom sort function, in reverse order of height
## def yHeightSort(i,j):
##     if j.get_height() > i.get_height(): return 1
##     else: return -1

## # loop through sort assign z-order based on sort
## for x_pos, patches in patch_at_x.iteritems():
##     if len(patches) == 1: continue
##     patches.sort(cmp=yHeightSort)
##     [patch.set_zorder(patches.index(patch)) for patch in patches]

## pylab.show()

# system-specific parameters and functions
import sys


## from numpy import random, arange
## from matplotlib import pyplot as plt


from matplotlib.ticker import FuncFormatter
from pylab import *


## random.seed(12)
## ydata = (random.randint(-1,4,10))

## ydata_a = ydata.cumsum()[2:]
## ydata = (random.randint(-1,4,10))
## ydata_b = ydata.cumsum()[2:]
## width = 0.8
## xdata = (arange(len(ydata_a))+0.5)-(width/2.0)



## fig = plt.figure()
## ax = fig.add_subplot(2,1,1)
## ax.bar(xdata, ydata_a,width,color='g')
## ax.xaxis.set_ticks(xdata+(width/2.0))
## ax.xaxis.set_ticklabels([ str(2001+ind) for ind in range(len(xdata))])
## [line.set_marker('None') for line in ax.get_xticklines()]

## ax = fig.add_subplot(2,1,2)
## for x, y_a, y_b in zip(xdata,ydata_a,ydata_b):
##     if abs(y_a)<abs(y_b):
##         ax.bar([x], [y_a],width,color='b')
##         ax.bar([x], [y_b],width,color='r')
##     else:
##         ax.bar([x], [y_a],width,color='b')
##         ax.bar([x], [y_b],width,color='r')
## ax.xaxis.set_ticks(xdata+(width/2.0))
## ax.xaxis.set_ticklabels([ str(2001+ind) for ind in range(len(xdata))])
## [line.set_marker('None') for line in ax.get_xticklines()]
## plt.savefig('example.pdf', format ='pdf')
## plt.show()





## Sequences read: 120485

## Qualities read: 120485

## Good sequences written:   0

## Empty sequences discarded:   0

## Low quality sequences discarded: 11486

## Empty or short sequences discarded:   0

## Sequences discarded because of being just polyA:   0

## Sequences discarded because of being just vector:   0





y_value = [120485, 120485, 120485-11486, 0, 11486, 0, 0, 0]

image_file = r'/media/disk-1/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/'

x_axes_ticks = [r'Tot fasta', r'Tot qual', r'Tot good', r'Empty', r'Low quality', u'Empty/\nshort', r'PolyA', r'Vectors']



def main():
    graphics(y_value, x_axes_ticks, image_file)
    return 0


# create graphics
def graphics(money, x_axes_ticks, image_file):
    
    x =  arange(8)
    #money = [1.5e5, 2.5e6, 5.5e6, 2.0e7]

    def millions(x, pos):
        'The two args are the value and tick position'
        return '$%1.1fM' % (x*1e-6)

    #formatter = FuncFormatter(millions)

    ax = subplot(111)
    
    bar(x, money)
    ## xticks( x + 0.5,  ('Bill', 'Fred', 'Mary', 'Sue') )
    xticks( x + 0.4,  x_axes_ticks)
    
    ax.set_title('Analyzed sequences', fontdict=None)
    
    plt.savefig(image_file + r'example.pdf', format ='pdf')
    show()





# execute the main function
if __name__ == "__main__":
    main()

# 
# graph_examples.py ends here

# get_coverage_list.py --- 
# 
# Filename: get_coverage_list.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Fri Mar 18 12:53:29 2011 (+0100)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

from utils.pipe_utils import PipeUtil
import os


def main():
    util = PipeUtil()

    cover_stat_handles = []
    headers = []
    
    cover_stat_handles.append(open(r"CDNA3-4_11-1020_0_out.ace.txt_stats.txt", r"r"))
    cover_stat_handles.append(open(r"CDNA3-4_11_2010_1_final_assembly.fasta_stats_(2).txt", r"r"))

    headers.append(u"mean_CDNA3-4_coverage_assembly_0_ace")
    headers.append(u"mean_CDNA3-4_coverage_assembly_1_sam")

    #coverage_list = util.get_read_coverages(cover_stat_handles[0])
    #for elem in coverage_list:
        #print elem

    out_path = r''
    out_table_name = r'CDNA3-4_from_coverages_intervals_table_100.csv'
    out_table = os.path.join(out_path, out_table_name)

    num_bins = 100

    util.get_read_coverages_table(out_table, headers, num_bins, *cover_stat_handles)
    
    return 0



# execute the main function
if __name__ == "__main__":
    main()



# 
# get_coverage_list.py ends here

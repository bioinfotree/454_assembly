#!/usr/bin/env bRscript

import(optparse)

opt_spec <- list(make_option(c("-c", "--column"), type="integer",  help="column number"))

usage = paste(
  "%prog [OPTIONS] COL... <TABLE",
  sep="\n")

parser   <- OptionParser(usage=usage, option_list=opt_spec)
parsed   <- parse_args(parser, positional_arguments=TRUE)
opts     <- parsed$options
args     <- parsed$args

if (length(args) == 0)
  stop("Unexpected argument number.")


CDNA1<-read.table(args[1], sep="\t", row.names=1)

t<-as.numeric(CDNA1[,opts$column])

summary(t)
#boxplot(t)
mean(t)
median(t)

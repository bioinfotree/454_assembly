### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/454_preprocessing
context prj/transposon

PRJ ?=


# functional
extern ../phase_1/$(PRJ)_assemblies/assembly_1_remaps/alignments/ as ALN_DIR

extern ../phase_1/$(PRJ)_assemblies/assembly_1_remaps/$(PRJ)_1_final_assembly.fasta as AS_1_UPAD_FA

extern ../phase_2/$(PRJ)_1_no_singletons.lst as AS_1_NOSING_LST

extern ../phase_2/$(PRJ)_1_singletons.lst as AS_1_SING_LST

## from *_0_assembly/*_1_d_info_contigstats.txt
extern ../phase_1/$(PRJ)_assemblies/assembly_0/$(PRJ)_0_assembly/$(PRJ)_0_d_info/$(PRJ)_0_info_contigstats.txt as AS_0_COSTAT_LST

## from *_1_assembly/*_0_d_info_contigstats.txt
extern ../phase_1/$(PRJ)_assemblies/assembly_1/$(PRJ)_1_assembly/$(PRJ)_1_d_info/$(PRJ)_1_info_contigstats.txt as AS_1_COSTAT_LST




# # testing
# ## 1
# ## from assembly_1_remaps
# extern ../../../../../../Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1_remaps/alignments/ as ALN_DIR

# extern ../../../../../../Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1_remaps/CDNA3-4_11_2010_1_final_assembly.fasta as AS_1_UPAD_FA

# extern ../phase_2/$(PRJ)_1_no_singletons.lst as AS_1_NOSING_LST

# extern ../phase_2/$(PRJ)_1_singletons.lst as AS_1_SING_LST

# ## from *_1_assembly/*_1_d_info_contigstats.txt
# extern ../../../../../../Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_0/CDNA3-4_11_2010_0_assembly/CDNA3-4_11_2010_0_d_info/CDNA3-4_11_2010_0_info_contigstats.txt as AS_0_COSTAT_LST

# ## from *_1_assembly/*_1_d_info_contigstats.txt
# extern ../../../../../../Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1/CDNA3-4_11_2010_1_assembly/CDNA3-4_11_2010_1_d_info/CDNA3-4_11_2010_1_info_contigstats.txt as AS_1_COSTAT_LST

# extern ../../../../../../Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1_remaps/coverages/CDNA3-4_11_2010_1_final_assembly.fasta_stats.txt as AS_REMAP_COSTAT_LST




$(PRJ)_1_as_unpad.fasta: $(AS_1_UPAD_FA)
	ln -sf $< $@

$(PRJ)_1_no_singletons.lst: $(AS_1_NOSING_LST)
	ln -sf $< $@

$(PRJ)_1_singletons.lst: $(AS_1_SING_LST)
	ln -sf $< $@

$(PRJ)_0_as_costat.lst: $(AS_0_COSTAT_LST)
	ln -sf $< $@

$(PRJ)_1_as_costat.lst: $(AS_1_COSTAT_LST)
	ln -sf $< $@



# coverage contigs assembly 1, metacontig assembly 2
$(PRJ)_%_as_unpad_nosing.cov: $(PRJ)_%_as_costat.lst
	paste --delimiter "\n" \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "stdev_xbase_cov:",$$0; } \
	}') > $@




# coverage contigs assembly 1 + assembly 2
THIS_ALN_DIR = alignments
# also sengletons are remapped. So only link SAMs from contigs
$(THIS_ALN_DIR): $(PRJ)_1_no_singletons.lst $(ALN_DIR)
	mkdir -p $(THIS_ALN_DIR) && \
	FILE_LST=`cut -f 1 $<` && \
	cd $(THIS_ALN_DIR) && \
	for FILE in $$FILE_LST ; do \
	ln -sf ../$^2/$$FILE.sam ; \
	done; \
	cd ..; \
	touch $@


COV_DIR = cov_1
$(PRJ)_1_as_unpad.fasta_stats.txt: $(PRJ)_1_as_unpad.fasta $(THIS_ALN_DIR)
	sam_coverage --reference $< --sam_dir $^2 --tmp_dir $(COV_DIR) --unique && \
	ln -sf ./$(COV_DIR)/$@


#$(PRJ)_1_as_unpad_nosing_remap.cov: $(AS_REMAP_COSTAT_LST)
$(PRJ)_1_as_unpad_nosing_remap.cov: $(PRJ)_1_as_unpad.fasta_stats.txt
	paste --delimiter "\n" \
	<(bawk '!/^[$$,\#+]/ { \
	print $$1, $$3; \
	}' $< | stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	print $$1, $$3; \
	}' $< | stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	print $$1, $$3; \
	}' $< | stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "stdev_xbase_cov:",$$0; } \
	}') > $@


# make a graph of frequencies of contigs for given bins of coverages for:
# first round assembly
# second round assembly
# final assembly (by joining first and second asseblies)

#$(PRJ)_all_as_unpad_distribplot_cov.pdf: $(PRJ)_0_as_costat.lst $(PRJ)_1_as_costat.lst $(AS_REMAP_COSTAT_LST)
$(PRJ)_all_as_unpad_distribplot_cov.pdf: $(PRJ)_0_as_costat.lst $(PRJ)_1_as_costat.lst $(PRJ)_1_as_unpad.fasta_stats.txt
	paste <(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$6; \
	} \
	}' $<) \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$6; \
	} \
	}' $^2) \
	<(bawk '!/^[$$,\#+]/ { \
	print $$3; \
	}' $^3) | \
	distrib_plot --type histogram --breaks 30 --remove-na --output $@ 1 2 3


# Unfortunately, most of the contigs are in the range of coverage 0-10 and 
# only a few contigs have extremely high coverage. 
# The resulting graph is squeezed toward low values.
# So it is better to customize the intervals to redistribute values. 
# It 'better to do that in LibreOffice calc. 
# I have a spreadsheet available with bin intervals that have been customized. 
# Use Binner with the same set of intervals, to generate the classification.

# prepare file with bins
bins.tab:
	make_bins > $@

# get classification with binner for upload in LibreOffice calc
$(PRJ)_all_as_unpad_distribplot_cov.tab: $(PRJ)_0_as_costat.lst $(PRJ)_1_as_unpad.fasta_stats.txt bins.tab
	paste <(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$6; \
	} \
	}' $< | binner --bin-from-file=$^3) \
	<(bawk '!/^[$$,\#+]/ { \
	print $$3; \
	}' $^2 | binner --bin-from-file=$^3) \
	> $@





.PHONY: test
test:
	@echo $(basename $(ALN_DIR))


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(PRJ)_0_as_unpad_nosing.cov \
	 $(PRJ)_1_as_unpad_nosing.cov \
	 $(PRJ)_1_as_unpad_nosing_remap.cov \
	 $(PRJ)_all_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_all_as_unpad_distribplot_cov.tab


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(PRJ)_1_as_unpad_nosing.cov \
	 $(PRJ)_1_as_unpad_nosing_remap.cov \
	 $(PRJ)_1_no_singletons.lst \
	 $(PRJ)_1_as_costat.lst \
	 $(PRJ)_1_as_unpad.fasta \
         $(PRJ)_1_as_unpad.fasta.fai \
         $(PRJ)_1_as_unpad.fasta_stats.txt \
	 $(PRJ)_1_as_unpad.fasta \
	 $(PRJ)_0_as_unpad_nosing.cov \
	 $(PRJ)_0_as_costat.lst \
	 $(PRJ)_all_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_all_as_unpad_distribplot_cov.tab


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf $(COV_DIR)
	$(RM) -rf $(THIS_ALN_DIR)

######################################################################
### phase_3.mk ends here

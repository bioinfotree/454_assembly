### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/454_preprocessing

PRJ ?=

# functional
## 0
## all from from *_0_assembly, MIRA fold
extern ../phase_1/$(PRJ)_assemblies/assembly_0/$(PRJ)_0_assembly/$(PRJ)_0_d_results/$(PRJ)_0_out.unpadded.fasta as AS_0_UPAD_FA

extern ../phase_1/$(PRJ)_assemblies/assembly_0/$(PRJ)_0_assembly/$(PRJ)_0_d_results/$(PRJ)_0_out.unpadded.fasta.qual as AS_0_UPAD_QA

extern ../phase_1/$(PRJ)_assemblies/assembly_0/$(PRJ)_0_assembly/$(PRJ)_0_d_info/$(PRJ)_0_info_contigreadlist.txt as AS_0_CORE_LST

## 1
## from assembly_1_remaps
extern ../phase_1/$(PRJ)_assemblies/assembly_1_remaps/$(PRJ)_1_final_assembly.fasta as AS_1_UPAD_FA

## from assembly_1_remaps
extern ../phase_1/$(PRJ)_assemblies/assembly_1_remaps/$(PRJ)_1_final_assembly.fasta.qual as AS_1_UPAD_QA

## from *_1_assembly/*_1_d_info_contigreadlist.txt
extern ../phase_1/$(PRJ)_assemblies/assembly_1/$(PRJ)_1_assembly/$(PRJ)_1_d_info/$(PRJ)_1_info_contigreadlist.txt as AS_1_CORE_LST

## from *_1_assembly/*_1_d_info_contigstats.txt
extern ../phase_1/$(PRJ)_assemblies/assembly_1/$(PRJ)_1_assembly/$(PRJ)_1_d_info/$(PRJ)_1_info_contigstats.txt as AS_1_COSTAT_LST




# # testing
# ## 0
# ## all from from *_0_assembly, MIRA fold
# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_0/CDNA3-4_11_2010_0_assembly/CDNA3-4_11_2010_0_d_results/CDNA3-4_11_2010_0_out.unpadded.fasta as AS_0_UPAD_FA

# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_0/CDNA3-4_11_2010_0_assembly/CDNA3-4_11_2010_0_d_results/CDNA3-4_11_2010_0_out.unpadded.fasta.qual as AS_0_UPAD_QA

# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_0/CDNA3-4_11_2010_0_assembly/CDNA3-4_11_2010_0_d_info/CDNA3-4_11_2010_0_info_contigreadlist.txt as AS_0_CORE_LST

# # testing
# ## 1
# ## from assembly_1_remaps
# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1_remaps/CDNA3-4_11_2010_1_final_assembly.fasta as AS_1_UPAD_FA

# ## from assembly_1_remaps
# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1_remaps/CDNA3-4_11_2010_1_final_assembly.fasta.qual as AS_1_UPAD_QA

# ## from *_1_assembly/*_1_d_info_contigreadlist.txt
# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1/CDNA3-4_11_2010_1_assembly/CDNA3-4_11_2010_1_d_info/CDNA3-4_11_2010_1_info_contigreadlist.txt as AS_1_CORE_LST


# ## from *_1_assembly/*_1_d_info_contigstats.txt
# extern /home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2010-12-14/CDNA3-4_11_2010/my_multipass_assemblies/assembly_1/CDNA3-4_11_2010_1_assembly/CDNA3-4_11_2010_1_d_info/CDNA3-4_11_2010_1_info_contigstats.txt as AS_1_COSTAT_LST




# must be added for every assembly round
## 0
$(PRJ)_0_as_unpad.fasta: $(AS_0_UPAD_FA)
	ln -sf $< $@

$(PRJ)_0_as_unpad.fasta.qual: $(AS_0_UPAD_QA)
	ln -sf $< $@

$(PRJ)_0_as_core.lst: $(AS_0_CORE_LST)
	ln -sf $< $@

## 1
$(PRJ)_1_as_unpad.fasta: $(AS_1_UPAD_FA)
	ln -sf $< $@

$(PRJ)_1_as_unpad.fasta.qual: $(AS_1_UPAD_QA)
	ln -sf $< $@

$(PRJ)_1_as_core.lst: $(AS_1_CORE_LST)
	ln -sf $< $@

$(PRJ)_1_as_costat.lst: $(AS_1_COSTAT_LST)
	ln -sf $< $@


# number of reads per contigs from MIRA contigreadlist
# select singletons (only one list)
$(PRJ)_%_singletons.lst: $(PRJ)_%_as_core.lst
	bawk -v cov=1 'BEGIN { i=0; j=0;} !/^[$$,\#+]/ \
	{ \
	# check at least 1 reads \
	if ($$2 == "") { exit; }; \
	# initialize with first value \
	if (j == 0 ) { frst == $$1 }; \
	if(frst != $$1) \
	{ \
	# coverage choice \
	if (i == cov) \
	{ \
	print frst, i; \
	} \
	frst=$$1; \
	i=0; \
	} \
	i++; \
	j++; \
	} END { \
	# print last singleton \
	if (i == 1) { print frst, i; } \
	}' $< > $@


# number of reads per contigs from MIRA contigreadlist
# select contigs (at least 2 reads)
$(PRJ)_%_no_singletons.lst: $(PRJ)_%_as_core.lst
	bawk -v cov=1 'BEGIN { i=0; j=0;} !/^[$$,\#+]/ \
	{ \
	# check at least 1 reads \
	if ($$2 == "") { exit; }; \
	# initialize with first value \
	if (j == 0 ) { frst == $$1 }; \
	if(frst != $$1) \
	{ \
	# coverage choice DIFFERENCE \
	if (i > cov) \
	{ \
	print frst, i; \
	} \
	frst=$$1; \
	i=0; \
	} \
	i++; \
	j++; \
	} END { \
	# print last contig DIFFERENCE \
	if (i > 1) { print frst, i; } \
	}' $< > $@



$(PRJ)_%_as_unpad_sing.fasta: $(PRJ)_%_singletons.lst $(PRJ)_%_as_unpad.fasta
	get_fasta -f <(cut -f 1 $<) < $^2 > $@

$(PRJ)_%_as_unpad_sing.fasta.qual: $(PRJ)_%_singletons.lst $(PRJ)_%_as_unpad.fasta.qual
	get_fasta -f <(cut -f 1 $<) < $^2 > $@


$(PRJ)_%_as_unpad_nosing.fasta: $(PRJ)_%_no_singletons.lst $(PRJ)_%_as_unpad.fasta
	get_fasta -f <(cut -f 1 $<) < $^2 > $@

$(PRJ)_%_as_unpad_nosing.fasta.qual: $(PRJ)_%_no_singletons.lst $(PRJ)_%_as_unpad.fasta.qual
	get_fasta -f <(cut -f 1 $<) < $^2 > $@


# get statistic about GC and total length singletons
$(PRJ)_%_as_unpad_sing.info: $(PRJ)_%_as_unpad_sing.fasta $(PRJ)_%_as_unpad_sing.fasta.qual $(PRJ)_%_singletons.lst
	$(call get_info,$<,$^2,$^3)


# get statistic about GC and total length contigs
$(PRJ)_%_as_unpad_nosing.info: $(PRJ)_%_as_unpad_nosing.fasta $(PRJ)_%_as_unpad_nosing.fasta.qual $(PRJ)_%_no_singletons.lst
	$(call get_info,$<,$^2,$^3)

# get statistic about GC and total length all
$(PRJ)_%_as_unpad.info: $(PRJ)_%_as_unpad.fasta $(PRJ)_%_as_unpad.fasta.qual $(PRJ)_%_no_singletons.lst $(PRJ)_%_singletons.lst
	cat $^3 $^4 > $(PRJ)_$*_all.lst && \
	$(call get_info,$<,$^2,$(PRJ)_$*_all.lst)



# $(call get_info, fasta, qual, contig reads_num)
define get_info
paste \
<(count_fasta -i 50 $1 ) \
<(fastalen $1 | \
stat_base --mean 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_length",$$0; } \
}') \
<(fastalen $1 | \
stat_base --stdev 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "standard_deviation_length",$$0; } \
}') \
<(fastalen $1 | \
stat_base --median 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "median_length",$$0; } \
}') \
<(qual_mean --total < $2 | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_quality",$$0; } \
}') \
<(stat_base --mean 2 < $3 | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_reads_cov",$$0; } \
}') > $@
endef




# get contig len>=100 qual>=30
# script help criptic; to be rebuild
$(PRJ)_%_as_unpad_200.info: $(PRJ)_%_as_unpad.fasta $(PRJ)_%_as_unpad.fasta.qual
	filter_by_quality_and_len -q 30 -l 200 -c $< > $@



$(PRJ)_1_as_unpad_meta.info: $(PRJ)_1_as_costat.lst
	paste -d "\n" \
	<(bawk '!/^[$$,\#+]/ { \
	{ \
	i=i+$$4; \
	}\
	} END { print "metacontig+singletons:",i; }' $<) \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	i=i+$$4; \
	}\
	} END { print "sequence_in_metacontigs:",i; }' $<) \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	i=i+$$2; \
	} \
	} END { print "total_consensus:",i; }' $<) \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$2; \
	} \
	}' $< | stat_base --mean 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_metacontigs_length:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$2; \
	} \
	}' $< | stat_base --median 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_metacontigs_length:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$2; \
	} \
	}' $< | stat_base --stdev 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "standard_deviation_metacontigs_length:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$7; \
	} \
	}' $< | stat_base --mean 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "GC_metacontigs_content_%:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$4; \
	} \
	}' $< | stat_base --mean 1 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_metacontigs_coverage:",$$0; } \
	}') > $@




$(PRJ)_1-2_as_unpad_distribplot_len.pdf: $(PRJ)_0_as_unpad_nosing.fasta $(PRJ)_0_as_unpad_sing.fasta $(PRJ)_1_as_unpad_nosing.fasta $(PRJ)_1_as_unpad_sing.fasta
	paste <(fastalen $< | cut -f2) \
	<(fastalen $^3 | cut -f2) \
	<(fastalen $^2 | cut -f2) \
	<(fastalen $^4 | cut -f2) | \
	qual_distrib_plot --type histogram --bin-size=150 --remove-na --title="Length distribution of sequences from first round and final assemblies" --xlab="length" --ylab="sequences (#)" --legend-label="contigs round 1","final contigs","singletons round 1","final sigletons" --color="orange","red","cyan","blue" --output $@ 1 2 3 4



$(PRJ)_1-2_as_unpad_distribplot_qual.pdf: $(PRJ)_0_as_unpad_nosing.fasta.qual $(PRJ)_0_as_unpad_sing.fasta.qual $(PRJ)_1_as_unpad_nosing.fasta.qual $(PRJ)_1_as_unpad_sing.fasta.qual
	paste <(qual_mean < $< | cut -f2) \
	<(qual_mean < $^3 | cut -f2) \
	<(qual_mean < $^2 | cut -f2) \
	<(qual_mean < $^4 | cut -f2) | \
	qual_distrib_plot --type histogram --bin-size=5 --remove-na --title="Distribution of average quality from the first round and final assemblies" --xlab="phred quality" --ylab="sequences (#)" --legend-label="contigs round 1","final contigs","singletons round 1","final sigletons" --color="orange","red","cyan","blue" --output $@ 1 2 3 4



$(PRJ)_%_as_unpad_distribplot_cov.pdf: $(PRJ)_%_no_singletons.lst
	cut -f2 $< | \
	distrib_plot --type histogram --breaks 10 --remove-na --output $@ 1




$(PRJ)_%_as_unpad_scatterplot_len-qual.pdf: $(PRJ)_%_as_unpad_nosing.fasta $(PRJ)_%_as_unpad_nosing.fasta.qual $(PRJ)_%_as_unpad_sing.fasta $(PRJ)_%_as_unpad_sing.fasta.qual
	paste <(fastalen $< | bsort --key=1,1 | cut -f2) \
	<(qual_mean < $^2 | bsort --key=1,1 | cut -f2) \
	<(fastalen $^3 | bsort --key=1,1 | cut -f2) \
	<(qual_mean < $^4 | bsort --key=1,1 | cut -f2) | \
	this_scatterplot --no-header --xlab="length (bp)" --ylab="av.qual (phred)" --title="Contigs length versus average quality" --color="red","blue" --pch 1,1 --legend-label="contigs","singletons" --output-file=$@ 1,2 3,4


$(PRJ)_%_as_unpad_nosing_scatterplot_len-cov.pdf: $(PRJ)_%_as_unpad_nosing.fasta $(PRJ)_%_no_singletons.lst
	paste <(fastalen <$< | bsort --key=1,1 | cut -f2) \
	<(bsort --key=1,1 <$^2 | cut -f2) | \
	this_scatterplot --no-header --xlab="length (bp)" --ylab="reads (#)" --title="Contigs length versus number of reads per contig" --color="red" --pch 1 --legend-label="contigs" --output-file=$@ 1,2


$(PRJ)_%_as_unpad_nosing_scatterplot_cov-qual.pdf: $(PRJ)_%_as_unpad_nosing.fasta.qual $(PRJ)_%_no_singletons.lst
	paste <(bsort --key=1,1 <$^2 | cut -f2) \
	<(qual_mean <$< | bsort --key=1,1 | cut -f2) | \
	this_scatterplot --no-header --ylab="av.qual (phred)" --xlab="reads (#)" --title="Contigs average quality versus number of reads per contig" --color="red" --pch 1 --legend-label="contigs" --output-file=$@ 1,2







.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(PRJ)_0_singletons.lst \
	 $(PRJ)_0_no_singletons.lst \
	 $(PRJ)_0_as_unpad_nosing.info \
	 $(PRJ)_0_as_unpad_sing.info \
	 $(PRJ)_0_as_unpad.info \
	 $(PRJ)_0_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_0_as_unpad_scatterplot_len-qual.pdf \
	 $(PRJ)_0_as_unpad_nosing_scatterplot_len-cov.pdf \
	 $(PRJ)_0_as_unpad_nosing_scatterplot_cov-qual.pdf \
	 $(PRJ)_1_as_unpad_scatterplot_len-qual.pdf \
	 $(PRJ)_1_as_unpad_nosing_scatterplot_len-cov.pdf \
	 $(PRJ)_1_as_unpad_nosing_scatterplot_cov-qual.pdf \
	 $(PRJ)_0_as_unpad_200.info \
	 $(PRJ)_1_singletons.lst \
	 $(PRJ)_1_no_singletons.lst \
	 $(PRJ)_1_as_unpad_nosing.info \
	 $(PRJ)_1_as_unpad_sing.info \
	 $(PRJ)_1_as_unpad.info \
	 $(PRJ)_1_as_unpad_meta.info \
	 $(PRJ)_1_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_1_as_unpad_200.info \
	 $(PRJ)_1-2_as_unpad_distribplot_len.pdf \
	 $(PRJ)_1-2_as_unpad_distribplot_qual.pdf


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(PRJ)_0_as_unpad.fasta \
	 $(PRJ)_0_as_unpad.fasta.qual \
	 $(PRJ)_0_as_core.lst \
	 $(PRJ)_0_singletons.lst \
	 $(PRJ)_0_no_singletons.lst \
	 $(PRJ)_0_as_unpad_sing.fasta \
	 $(PRJ)_0_as_unpad_nosing.fasta \
	 $(PRJ)_0_as_unpad_nosing.info \
	 $(PRJ)_0_as_unpad_sing.info \
	 $(PRJ)_0_as_unpad.info \
	 $(PRJ)_0_all.lst \
	 $(PRJ)_0_as_unpad_200.info \
	 $(PRJ)_1_as_unpad_nosing.info \
	 $(PRJ)_1_as_unpad_sing.info \
	 $(PRJ)_1_as_unpad.info \
	 $(PRJ)_1_as_core.lst \
	 $(PRJ)_1_as_unpad.fasta \
	 $(PRJ)_1_as_unpad.fasta.qual \
	 $(PRJ)_1_no_singletons.lst \
	 $(PRJ)_1_singletons.lst \
	 $(PRJ)_1_all.lst \
	 $(PRJ)_1_as_unpad_meta.info \
	 $(PRJ)_0_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_1_as_costat.lst \
	 $(PRJ)_1_as_unpad_distribplot_cov.pdf \
	 $(PRJ)_1_as_unpad_200.info \
	 $(PRJ)_1-2_as_unpad_distribplot_len.pdf \
	 $(PRJ)_1-2_as_unpad_distribplot_qual.pdf \
	 $(PRJ)_0_as_unpad_nosing.fasta.qual \
	 $(PRJ)_0_as_unpad_sing.fasta.qual \
	 $(PRJ)_1_as_unpad_nosing.fasta \
         $(PRJ)_1_as_unpad_nosing.fasta.qual \
         $(PRJ)_1_as_unpad_sing.fasta \
         $(PRJ)_1_as_unpad_sing.fasta.qual \
	 $(PRJ)_0_as_unpad_scatterplot_len-qual.pdf \
	 $(PRJ)_0_as_unpad_nosing_scatterplot_len-cov.pdf \
	 $(PRJ)_0_as_unpad_nosing_scatterplot_cov-qual.pdf \
	 $(PRJ)_1_as_unpad_scatterplot_len-qual.pdf \
	 $(PRJ)_1_as_unpad_nosing_scatterplot_len-cov.pdf \
	 $(PRJ)_1_as_unpad_nosing_scatterplot_cov-qual.pdf

######################################################################
### phase_1.mk ends here

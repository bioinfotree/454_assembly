### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


context prj/transposon

PRJ ?=

INPUT_FASTA ?=

INPUT_QUAL ?=

INPUT_XML ?=

INPUT_MIRA_STRAINDATA ?=



# do not place links to these files, otherwise the program no longer works
454_INPUT += $(INPUT_FASTA) $(INPUT_QUAL) $(INPUT_XML) $(INPUT_MIRA_STRAINDATA)


# put all results in a folder
# scorporare in task unico per soli assemblaggi
PRJ_ASSEMBLIES = $(addsuffix _assemblies, $(PRJ))
$(PRJ_ASSEMBLIES): $(454_INPUT) 
	mkdir -p $(PRJ_ASSEMBLIES) && cd $(PRJ_ASSEMBLIES) && \
	make_repeated_assembly --fasta ../$< --qual ../$^2 --xml ../$^3 --strain ../$^4 --cpus 4 --prj $(PRJ) --cycles 2 && \
	cd ..


# join all sam files for a given assembly round remap 
# created by make_repeated_assembly
# return a single sam file with header on STDOUT
define merge_sams
	find ./$1/assembly_$2_remaps/alignments/ -name '*.sam' \
	| { read firstbam; \
	samtools view -b -T ./$1/assembly_$2_remaps/$(PRJ)_$2_final_assembly.fasta "$$firstbam" \
	| samtools view -h -; \
	while read bam; do \
	samtools view -T ./$1/assembly_$2_remaps/$(PRJ)_$2_final_assembly.fasta "$$bam"; \
	done }
endef



assembly_%_aln.sam: $(PRJ_ASSEMBLIES)
	$(call merge_sams,$<,$*) > $@

# covert unique sam to bam, sort and index it
assembly_%_aln.bam: $(PRJ_ASSEMBLIES)
	$(call merge_sams,$<,$*) \
	| samtools view -ubS - \
	| samtools sort - $(basename $@); \
	samtools index $@







.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(PRJ_ASSEMBLIES) \
	 assembly_1_aln.sam \
	 assembly_1_aln.bam

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += assembly_1_aln.sam \
	 assembly_1_aln.bam \
	 assembly_1_aln.bam.bai



######################################################################
### phase_1.mk ends here
